<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcademicOffersByEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('academic_offers_by_events', function (Blueprint $table) {
            $table->unsignedBigInteger('academic_offer_id');
            $table->unsignedBigInteger('event_id');
            $table->foreign('academic_offer_id')->references('id')->on('academic_offers')->onDelete('cascade');
            $table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');
            $table->primary(['academic_offer_id','event_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('academic_offers_by_events',function(Blueprint $t){
            $t->dropForeign('academic_offers_by_events_academic_offer_id_foreign');
            $t->dropForeign('academic_offers_by_events_event_id_foreign');
        });
        Schema::dropIfExists('academic_offers_by_events');
    }
}

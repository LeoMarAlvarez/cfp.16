<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableCommissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commissions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',20);
            $table->unsignedBigInteger('offer_id');
            $table->unsignedBigInteger('teacher_id')->nullable();
            $table->string('schedule');
            $table->foreign('offer_id')->references('id')->on('academic_offers')->onDelete('cascade');
            $table->foreign('teacher_id')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('commissions', function(Blueprint $table){
            $table->dropForeign('commissions_offer_id_foreign');
            $table->dropForeign('commissions_teacher_id_foreign');
        });
        Schema::dropIfExists('table_commissions');
    }
}

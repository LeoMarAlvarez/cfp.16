<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcademicOffersByPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('academic_offers_by_posts', function (Blueprint $table) {
            $table->unsignedBigInteger('academic_offer_id');
            $table->unsignedBigInteger('post_id');
            $table->foreign('academic_offer_id')->references('id')->on('academic_offers')->onDelete('cascade');
            $table->foreign('post_id')->references('id')->on('posts')->onDelete('cascade');
            $table->primary(['academic_offer_id','post_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('academic_offers_by_posts',function(Blueprint $t){
            $t->dropForeign('academic_offers_by_posts_academic_offer_id_foreign');
            $t->dropForeign('academic_offers_by_posts_post_id_foreign');
        });
        Schema::dropIfExists('academic_offers_by_posts');
    }
}

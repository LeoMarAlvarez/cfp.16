<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonalsDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personals_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('last_name',100)->nullable();
            $table->string('dni',8)->unique();
            $table->string('phone',20)->nullable();
            $table->string('number_file')->nullable();
            $table->string('photo')->nullable();
            $table->text('description')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('personals_data',function(Blueprint $t){
            $t->dropForeign('personals_data_user_id_foreign');
        });
        Schema::dropIfExists('personals_data');
    }
}

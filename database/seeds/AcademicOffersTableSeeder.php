<?php

use Illuminate\Database\Seeder;
use App\Models\AcademicOffer;
use App\Models\Commission;

class AcademicOffersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $programacion = AcademicOffer::create([
            'name' =>'programacion',
            'graduate_profile' => 'programacion',
            'description' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Accusamus exercitationem quos ullam? Nemo autem expedita pariatur et eos! Ipsam blanditiis suscipit fugiat incidunt! Omnis, inventore. Quas cum porro voluptatibus. Exercitationem?',
            'content' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Accusamus exercitationem quos ullam? Nemo autem expedita pariatur et eos! Ipsam blanditiis suscipit fugiat incidunt! Omnis, inventore. Quas cum porro voluptatibus. Exercitationem?',
            'duration' => '1 año',
            'attached_program' => 'ofertas/programacion.pdf',
            'photo' => 'ofertas/programacion.jpg'
        ]);

        $chef = AcademicOffer::create([
            'name' =>'chef',
            'graduate_profile' => 'chef',
            'description' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Accusamus exercitationem quos ullam? Nemo autem expedita pariatur et eos! Ipsam blanditiis suscipit fugiat incidunt! Omnis, inventore. Quas cum porro voluptatibus. Exercitationem?',
            'content' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Accusamus exercitationem quos ullam? Nemo autem expedita pariatur et eos! Ipsam blanditiis suscipit fugiat incidunt! Omnis, inventore. Quas cum porro voluptatibus. Exercitationem?',
            'duration' => '1 año',
            'attached_program' => 'ofertas/chef.pdf',
            'photo' => 'ofertas/chef.jpg'
        ]);

        $peluqueria = AcademicOffer::create([
            'name' =>'peluqueria',
            'graduate_profile' => 'peluqueria',
            'description' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Accusamus exercitationem quos ullam? Nemo autem expedita pariatur et eos! Ipsam blanditiis suscipit fugiat incidunt! Omnis, inventore. Quas cum porro voluptatibus. Exercitationem?',
            'content' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Accusamus exercitationem quos ullam? Nemo autem expedita pariatur et eos! Ipsam blanditiis suscipit fugiat incidunt! Omnis, inventore. Quas cum porro voluptatibus. Exercitationem?',
            'duration' => '1 año',
            'attached_program' => 'ofertas/peluqueria.pdf',
            'photo' => 'ofertas/peluqueria.jpg'
        ]);

        $electricidad = AcademicOffer::create([
            'name' =>'electricidad',
            'graduate_profile' => 'electricidad',
            'description' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Accusamus exercitationem quos ullam? Nemo autem expedita pariatur et eos! Ipsam blanditiis suscipit fugiat incidunt! Omnis, inventore. Quas cum porro voluptatibus. Exercitationem?',
            'content' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Accusamus exercitationem quos ullam? Nemo autem expedita pariatur et eos! Ipsam blanditiis suscipit fugiat incidunt! Omnis, inventore. Quas cum porro voluptatibus. Exercitationem?',
            'duration' => '1 año',
            'attached_program' => 'ofertas/electricidad.pdf',
            'photo' => 'ofertas/electricidad.jpg'
        ]);

        $secretariado = AcademicOffer::create([
            'name' =>'secretariado',
            'graduate_profile' => 'secretariado',
            'description' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Accusamus exercitationem quos ullam? Nemo autem expedita pariatur et eos! Ipsam blanditiis suscipit fugiat incidunt! Omnis, inventore. Quas cum porro voluptatibus. Exercitationem?',
            'content' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Accusamus exercitationem quos ullam? Nemo autem expedita pariatur et eos! Ipsam blanditiis suscipit fugiat incidunt! Omnis, inventore. Quas cum porro voluptatibus. Exercitationem?',
            'duration' => '1 año',
            'attached_program' => 'ofertas/secretariado.pdf',
            'photo' => 'ofertas/secretariado.jpg'
        ]);

        $cUno = Commission::create([
            'name' => 'A',
            'schedule' => 'Lunes Miércoles y Viernes de 14:00hs a 17:00hs',
            'teacher_id' => 2,
            'offer_id' => 1
        ]);

        $cDos = Commission::create([
            'name' => 'B',
            'schedule' => 'Martes Jueves de 14:00hs a 17:00hs y Viernes de 17:00 a 20:00hs',
            'teacher_id' => 3,
            'offer_id' => 1
        ]);

        $cTres = Commission::create([
            'name' => 'A',
            'schedule' => 'Lunes Miércoles y Viernes de 14:00hs a 17:00hs',
            'teacher_id' => 3,
            'offer_id' => 3
        ]);

        $cCuatro = Commission::create([
            'name' => 'A',
            'schedule' => 'Martes Jueves de 14:00hs a 17:00hs y Viernes de 17:00 a 20:00hs',
            'teacher_id' => 2,
            'offer_id' => 5
        ]);

        $cCinco = Commission::create([
            'name' => 'A',
            'schedule' => 'Martes Jueves de 14:00hs a 17:00hs',
            'teacher_id' => 2,
            'offer_id' => 2
        ]);

        $cSeis = Commission::create([
            'name' => 'A',
            'schedule' => 'Martes Jueves de 17:00hs a 20:00hs',
            'teacher_id' => 3,
            'offer_id' => 4
        ]);
    }
}

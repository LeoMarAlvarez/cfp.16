<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $d = Role::create([
            'name'=>'Director'
        ]);

        $s = Role::create([
            'name' => 'Secretario'
        ]);

        $p = Role::create([
            'name' => 'Profesor'
        ]);

        $a = Role::create([
            'name' => 'Alumno'
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Models\PersonalData;
use App\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $u = User::create([
            'name' => 'Lucas',
            'email' => 'a@a.com',
            'password' => bcrypt('1234')
        ]);

        $juan = User::create([
            'name' => 'Juan',
            'email'=>'juan@prueba.com',
            'password'=>bcrypt('1234')
        ]);

        $p = User::create([
            'name'=>'Pablo',
            'email'=>'pablo@prueba.com',
            'password' => bcrypt('1234')
        ]);

        $l = User::create([
            'name'=>'Leo',
            'email'=>'leo@prueba.com',
            'password' => bcrypt('1234')
        ]);

        $juan->assignRole(Role::find(3));
        $p->assignRole(Role::find(3));
        $u->assignRole(Role::find(1));
        $l->assignRole(Role::find(1));

        $up = PersonalData::create([
            'last_name'=>'Ferrerya De Moraiz',
            'dni'=>'34100200',
            'phone'=>'376-4210101',
            'number_file'=>'12345',
            'user_id'=>$u->id
        ]);
        $juanp = PersonalData::create([
            'last_name'=>'Perez',
            'dni'=>'29908721',
            'phone'=>'-',
            'number_file'=>'-',
            'user_id'=>$juan->id
        ]);
        $pp = PersonalData::create([
            'last_name'=>'Sanchez',
            'dni'=>'25908156',
            'phone'=>'-',
            'number_file'=>'-',
            'user_id'=>$p->id
        ]);
        $lp = PersonalData::create([
            'last_name'=>'Alvarez',
            'dni'=>'34447745',
            'phone'=>'376-4323679',
            'number_file'=>'-',
            'user_id'=>$l->id
        ]);
    }
}

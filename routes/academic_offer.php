<?php

Route::middleware(['auth'])->prefix('administracion/')->group(function(){
    Route::resource('ofertas-academicas','AcademicOfferController')->names([
        'index' => 'offer.index',
        'create' => 'offer.create',
        'store' => 'offer.store',
        'edit' => 'offer.edit',
        'update' => 'offer.update',
        'show' => 'offer.show',
        'destroy' => 'offer.destroy',
    ]);

    Route::get('ofertas-academicas-list','AcademicOfferController@list')->name('offer.list');
});

Route::get('/ofertas-academicas','AcademicOfferController@showAllOffers')->name('showAllOffers');
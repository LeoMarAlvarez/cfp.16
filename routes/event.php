<?php

Route::resource('administración/eventos','EventController')->names([
    'index' => 'event.index',
    'create' => 'event.create',
    'store' => 'event.store',
    'edit' => 'event.edit',
    'update' => 'event.update',
    'show' => 'event.show',
    'destroy' => 'event.destroy'
])->middleware('auth');

Route::get('eventos','EventController@showAllEvents')->name('showAllEvents');
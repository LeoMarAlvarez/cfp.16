<?php

Route::resource('administración/posteos','PostController')->names([
    'index' => 'post.index',
    'create' => 'post.create',
    'store' => 'post.store',
    'edit' => 'post.edit',
    'update' => 'post.update',
    'show' => 'post.show',
    'destroy' => 'post.destroy'
])->middleware('auth');

Route::get('posts','PostController@showAllposts')->name('showAllposts');
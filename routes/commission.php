<?php

Route::middleware(['auth'])->prefix('administracion/')->group(function(){
    Route::resource('commission','CommissionController')->names([
        'index' => 'commission.index',
        'create' => 'commission.create',
        'store' => 'commission.store',
        'edit' => 'commission.edit',
        'update' => 'commission.update',
        'show' => 'commission.show',
        'destroy' => 'commission.destroy',
    ]);

    Route::get('commission-list','CommissionController@list')->name('commission.list');
});
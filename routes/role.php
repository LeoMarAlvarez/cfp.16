<?php

Route::resource('administración/roles','RoleController')->names([
    'index' => 'role.index',
    'create' => 'role.create',
    'store' => 'role.store',
    'edit' => 'role.edit',
    'update' => 'role.update',
    'show' => 'role.show',
    'destroy' => 'role.destroy'
])->middleware('auth');
<?php

Route::middleware('auth')->prefix('administracion')->group(function(){
    Route::resource('usuarios','UserController')->names([
        'index' => 'user.index',    //muestra todos los usuarios
        'create' => 'user.create',  //redirije a la vista de crear
        'store' => 'user.store',    //ruta que va en el form de create
        'edit' => 'user.edit',  //redirije a la vista edit
        'update' => 'user.update',  //ruta que va en el form del edit
        'show' => 'user.show',  //redirije al show
        'destroy' => 'user.destroy' //elimina
    ]);

    Route::get('user/list','UserController@list')->name('user.list');
});

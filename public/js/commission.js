$(document).ready(function() {
    var cC = $('#cajaComisiones');
    //lista todas las comisiones de la oferta cC.data('commission')
    listarComisiones(cC.data('list'), cC.data('commission'));

    //formulario para nueva Comision
    $('#nuevaComision').click(function(e) {
        e.preventDefault();
        var a = $(this);
        $.get({
            url: a.attr('href'),
            success: function(data) {
                $('#accionar').data('send', a.data('send'));
                $('#accionar').data('type', 'POST');
                cC.empty().html(data);
            }
        });
    });

    //cancelar formulario
    $(this).on('click', '#btnCancelarComision', function(e) {
        e.preventDefault();
        listarComisiones(cC.data('list'), cC.data('commission'));
    });

    //enviar formulario
    $(this).on('click', '#btnGuardarComision', function(e) {
        e.preventDefault();
        guardar();
    });

    //Editar una comision
    $(this).on('click', '.editarComision', function(e) {
        e.preventDefault();
        var a = $(this);
        $.get({
            url: a.attr('href'),
            success: function(data) {
                cC.empty().html(data);
                $('#accionar').data('send', a.data('send'));
                $('#accionar').data('type', 'PUT');
            }
        });
    });

    //Eliminar Comision
    $(this).on('click', '.borrarComision', function(e) {
        e.preventDefault();
        var a = $(this);
        confirmarBorradoComision(a.attr('href'));
    });
});

function listarComisiones(url, id) {
    $.get({
        url: url + '?id=' + id,
        success: function(data) {
            $('#cajaComisiones').empty().html(data);
            $('#accionar').data('send', ' ');
            $('#accionar').data('type', ' ');
        }
    });
}

function guardar() {
    var form = $('#formularioComision');
    offer = $('#cajaComisiones').data('commission');
    form.append(`<input class="d-none" name="offer_id" value="${offer}" />`);
    $.ajax({
        url: $('#accionar').data('send'),
        method: $('#accionar').data('type'),
        data: form.serialize(),
        success: function(data) {
            alerta(data);
            listarComisiones($('#cajaComisiones').data('list'), $('#cajaComisiones').data('commission'));
        },
        error: function(jqXhr) {
            if (jqXhr.status === 401)
                $(location).prop('pathname', 'auth/login');
            if (jqXhr.status === 422) {
                $errors = jqXhr.responseJSON;
                errorsHtml = '<div class="alert alert-default-warning"><ul>';
                $.each($errors.errors, function(key, value) {
                    errorsHtml += '<li>' + value[0] + '</li>';
                });
                errorsHtml += '</ul></div>';
                $('#response').html(errorsHtml);
            }
        }
    });
}

function confirmarBorradoComision(url) {
    swal({
        title: "Esta seguro de borrar el registro?",
        text: "Se borrara el registro",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Borrar",
        closeOnConfirm: false,
        closeOnCancel: false
    }, function(isConfirm) {
        if (isConfirm) {
            $.ajax({
                url: url,
                method: 'DELETE',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    alerta(data);
                    listarComisiones($('#cajaComisiones').data('list'), $('#cajaComisiones').data('commission'));
                },
                error: function(data) {
                    var message = {
                        'alert-type': 'error',
                        'message': 'No se pudo realizar la operación. Contactate con el administrador del sitio'
                    };
                    alerta(message);
                }
            });
        } else {
            swal({
                title: 'Cancelado',
                text: 'No se borrará el registro',
                type: 'error',
                showConfirmButton: false,
                timer: 1500
            });
        }
    });
}
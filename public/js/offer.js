$(document).ready(function() {
    var caja = $('#contenedor');

    //inicia y carga la lista de ofertas
    listar(caja.data('list'), '');

    //buscar
    $(this).on('keyup', '#search', function() {
        listar(caja.data('list'), $(this).val());
    });

    //nuevo
    $(this).on('click', '#nuevo', function(e) {
        e.preventDefault();
        var link = $(this);
        $.get(link.attr('href'), function(data) {
            bootbox.dialog({
                size: 'large',
                title: '<h2>' + link.data('title') + '</h2>',
                message: data,
                buttons: {
                    cancel: {
                        label: 'Cancelar',
                        className: 'btn-default'
                    },
                    noclose: {
                        label: 'Guardar',
                        className: 'btn-primary',
                        callback: function() {
                            guardar(link.data('send'), 'POST');
                            return false;
                        }
                    }
                }
            });
        });
    });

    //editar
    $(this).on('click', '.editar', function(e) {
        e.preventDefault();
        var link = $(this);
        console.log(link.attr('href'));
        $.get(link.attr('href'), function(data) {
            bootbox.dialog({
                size: 'large',
                title: '<h2>' + link.data('title') + '</h2>',
                message: data,
                buttons: {
                    cancel: {
                        label: 'Cancelar',
                        className: 'btn-default'
                    },
                    noclose: {
                        label: 'Guardar',
                        className: 'btn-primary',
                        callback: function() {
                            guardar(link.data('send'), 'PUT');
                            return false;
                        }
                    }
                }
            });
        });
    });

    //eliminar
    $(this).on('click', '.borrar', function(e) {
        e.preventDefault();
        confirmarBorrado($(this).attr('href'));
    });

    $(this).on('click', '.editar-comisiones', function(e) {
        e.preventDefault();
        a = $(this);
        $.get({
            url: a.attr('href') + '?id=' + a.data('commission'),
            method: 'get',
            success: function(data) {
                bootbox.alert({
                    title: 'comisiones',
                    size: 'large',
                    message: data,
                    buttons: {
                        ok: {
                            label: 'Salir',
                            className: 'btn-default'
                        }
                    },
                    callback: function() {
                        listar(caja.data('list'), '');
                    }
                });
            }
        });
    });

});

function listar(url, search) {
    $.ajax({
        url: url + '?search=' + search,
        method: 'GET',
        success: function(data) {
            $('#contenedor').empty().html(data);
        }
    });
}

function guardar(url, type) {
    var form = new FormData(document.getElementById('formulario'));
    if (type == 'PUT') {
        form.append('_method', 'PUT');
    }
    $.post({
        url: url,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: form,
        contentType: false,
        cache: false,
        processData: false,
        success: function(data) {
            alerta(data);
            listar($('#contenedor').data('list'), '');
            bootbox.hideAll();
        },
        error: function(jqXhr) {
            if (jqXhr.status === 401)
                $(location).prop('pathname', 'auth/login');
            if (jqXhr.status === 422) {
                $errors = jqXhr.responseJSON;
                errorsHtml = '<div class="alert alert-default-warning"><ul>';
                $.each($errors.errors, function(key, value) {
                    errorsHtml += '<li>' + value[0] + '</li>';
                });
                errorsHtml += '</ul></div>';
                $('#response').html(errorsHtml);
            }
        }
    });
}
@extends('layouts.backlog.app')

@section('otroslinks')
  <!-- Nuevo Post -->
  <!-- Formulario -->
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/select2/css/select2.min.css')}}">
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
  <!-- Editor -->
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/summernote/summernote-bs4.css')}}">
@endsection

@section('content')
	<!-- Content Wrapper. Contains page content -->
 	<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Nuevo Post</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Posts</a></li>
              <li class="breadcrumb-item active">Nuevo Post</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-outline card-info">
						<form class="form-horizontal">
							<div class="row">
                <div class="col-sm-6">
                  <!-- text input -->
                  <div class="form-group">
                    <label>Titulo</label>
                    <input type="text" class="form-control" placeholder="Titulo del post">
                  </div>
                </div>
							</div>

            	<div class="card-body pad">
              	<div class="mb-3">
                	<textarea class="textarea" placeholder="Escriba aqui el texto"
                          style="width: 100%; height: 500px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
              	</div>

              	<div class="col-md-6">
                	<div class="form-group">
                  	<label>Etiquetas</label>
                  	<select class="select2" multiple="multiple" style="width: 100%;">
	                    <option>Cursos</option>
	                    <option>Eventos</option>
	                    <option>Información</option>
	                    <option>Noticias</option>
	                    <option>Trabajo</option>
                  	</select>
                	</div>
                </div>

			  				<div class="card-footer">
                  <button type="submit" class="btn btn-default float-left">Cancelar</button>
                  <button type="submit" class="btn btn-info">Guardar</button>
                  <button type="submit" class="btn btn-success float-right">Subir</button>
                </div>
            	</div>
            </form>

            <p class="text-sm mb-0">
              <small>Ayuda <a href="#">Documentacion y manual de usuario</a></small>
            </p>
					</div>
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection

@section('otrosscripts')
	<!-- Nuevo Post -->
	<!-- Formulario -->
	<script src="{{ asset('adminlte/plugins/select2/js/select2.full.min.js')}}"></script>
	<!-- Editor -->
	<!-- Summernote -->
	<script src="{{ asset('adminlte/plugins/summernote/summernote-bs4.min.js')}}"></script>
	<script>
   	$(function () {
	    //Initialize Select2 Elements
  	  $('.select2').select2()
  	})

	  $(function () {
	    // Summernote
	    $('.textarea').summernote()
	  })
	</script>
@endsection

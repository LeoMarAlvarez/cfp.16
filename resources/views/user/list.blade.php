<table class="table table-striped table-bordered table-hover">
    <thead class="thead-dark">
        <tr>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Tipo</th>
            <th>Opción</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($users as $u)
            <tr>
                <td>{{$u->name}}</td>
                <td>{{$u->personalData->last_name}}</td>
                <td>{{$u->getRole()->name}}</td>
                <td>
                    <a href="{{route('user.show',$u->id)}}" 
                        class="btn btn-xs btn-primary ver"
                        title="Ver">
                        <i class="fa fa-eye"></i>
                    </a>
                    <a href="{{route('user.edit',$u->id)}}" 
                        class="btn btn-xs btn-success editar"
                        data-send="{{route('user.update',$u->id)}}"
                        data-title="Editar usuario {{$u->name.' '.$u->personalData->last_name}}"
                        data-type="PUT"
                        title="Editar">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{route('user.destroy',$u->id)}}" 
                        class="btn btn-xs btn-danger borrar"
                        title="Borrar">
                        <i class="fa fa-trash"></i>
                    </a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

<div class="card-footer">
    {!! $users->links() !!}
</div>
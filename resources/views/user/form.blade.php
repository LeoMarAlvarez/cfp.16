<form id="formulario" autocomplete="off">
    <div class="row">
        <div class="form-group col-lg-6">
            <label for="name">Nombre</label>
            <input 
                type="text" 
                name="name" 
                id="name" 
                class="form-control" 
                placeholder="Nombre"
                value="{{ (isset($usuario)? $usuario->name:'') }}">
        </div>
        <div class="form-group col-lg-6">
            <label for="last_name">Apellido</label>
            <input 
                type="text" 
                name="last_name" 
                id="last_name" 
                class="form-control" 
                placeholder="Apellido"
                value="{{ (isset($usuario->personalData)? $usuario->personalData->last_name:'') }}">
        </div>
        <div class="form-group col-lg-6">
            <label for="dni">DNI</label>
            <input 
                type="text" 
                name="dni" 
                id="dni" 
                class="form-control" 
                placeholder="34123456"
                value="{{ (isset($usuario->personalData)? $usuario->personalData->dni:'') }}">
        </div>
        <div class="form-group col-lg-6">
            <label for="number_file">N° de Legajo</label>
            <input 
                type="text" 
                name="number_file" 
                id="number_file" 
                class="form-control" 
                placeholder="1234"
                value="{{ (isset($usuario->personalData)? $usuario->personalData->number_file:'') }}">
        </div>
        <div class="form-group col-lg-8">
            <label for="email">Email</label>
            <input 
                type="text" 
                name="email" 
                id="email" 
                class="form-control" 
                placeholder="prueba@prueba.com"
                value="{{ (isset($usuario)? $usuario->email:'') }}">
        </div>
        <div class="form-group col-lg-6">
            <label for="phone">Teléfono</label>
            <input 
                type="text" 
                name="phone" 
                id="phone" 
                class="form-control" 
                placeholder="376-4466666"
                value="{{ (isset($usuario->personalData)? $usuario->personalData->phone:'') }}">
        </div>
        
        <div class="form-group col-lg-6">
            <label for="role">Tipo de Usuario</label>
            <select name="role" id="role" class="select2 form-control">
                @foreach($roles as $r)
                    <option value="{{$r->id}}" {{isset($usuario)? (($rol==$r->id)?'selected':''):''}}>{{$r->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="alert-danger" id="response"></div>
</form>
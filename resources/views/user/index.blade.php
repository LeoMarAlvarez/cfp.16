@extends('layouts.backlog.app')

@section('header-title')
    <h1>Usuarios</h1>
@endsection

@section('header-button')
    <a href="{{route('user.create')}}"
        data-send="{{route('user.store')}}"
        data-title="Nuevo Usuario"
        data-type="POST"
        class="btn btn-primary"
        id="nuevo">
        <i class="fa fa-plus"></i>
        Nuevo
    </a>
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <input type="text" id="search" class="form-control col-lg-4" autocomplete="off" placeholder="Buscar">
        </div>
        <div 
            class="card-body"
            id="contenedor"
            data-list="{{route('user.list')}}">
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('js/offer.js')}}"></script>
@endsection
  
  <!--==========================
    Header
  ============================-->
  <header id="header">
    <div class="container-fluid">

      <div id="logo" class="pull-left">
        <h1><a href="{{ route('welcome') }}" class="menu-active">CFP 16</a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="#intro"><img src="img/logo.png" alt="" title="" /></a>-->
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li><a href="{{ route('showAllOffers') }}">Ofertas Academicas</a></li>
          <li><a href="{{ route('showAllEvents') }}">Eventos</a></li>
          <li><a href="{{ route('showAllposts') }}">Blog</a></li>
          <!--
          <li class="menu-has-children"><a href="">Drop Down</a>
            <ul>
              <li><a href="#">Drop Down 1</a></li>
              <li><a href="#">Drop Down 3</a></li>
              <li><a href="#">Drop Down 4</a></li>
              <li><a href="#">Drop Down 5</a></li>
            </ul>
          </li>
          -->
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- #header -->
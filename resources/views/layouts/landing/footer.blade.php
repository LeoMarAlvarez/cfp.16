  <!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-info">
            <h3>C.F.P N°16</h3>
            <p>Cras fermentum odio eu feugiat lide par naso tierra. Justo eget nada terra videa magna derita valies darta donna mare fermentum iaculis eu non diam phasellus. Scelerisque felis imperdiet proin fermentum leo. Amet volutpat consequat mauris nunc congue.</p>
          </div>

          <div class="col-lg-3 col-md-6 footer-newsletter">
            <h4>Brindamos</h4>
            <p>Cursos con Rápida salida laboral. Certificados Oficiales. Convenios con distintas Instituciones Educativas y Sindicatos.</p>
            <!--
            <form action="" method="post">
              <input type="email" name="email"><input type="submit"  value="Subscribe">
            </form>
            -->
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Links Utiles</h4>
            <ul>
              <li><i class="ion-ios-arrow-right"></i> <a href="#">1</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="#">2</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="#">Oferta Academica</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="#">Requisitos de Inscripcion</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="{{route('login')}}">Iniciar Cesion</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-contact">
            <h4>Contactanos</h4>
            <p>
              Av. Centenario N°2723 <br>
              Posadas, Misiones (3300)<br>
              Argentina<br>
              <strong>Telefono:</strong> 0376 -443511<br>
              <strong>E-mail:</strong> cfp16@hotmail.com.ar<br>
            </p>

            <div class="social-links">
              <a href="https://twitter.com/cfp161" class="twitter"><i class="fa fa-twitter"></i></a>
              <a href="https://www.facebook.com/Centro-de-Formaci%C3%B3n-Profesional-N16-972193826161801/" class="facebook"><i class="fa fa-facebook"></i></a>
              <a href="https://www.instagram.com/formacionprofesional16/" class="instagram"><i class="fa fa-instagram"></i></a>
              <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
            </div>
          </div>
          
        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>BizPage</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
          All the links in the footer should remain intact.
          You can delete the links only if you purchased the pro version.
          Licensing information: https://bootstrapmade.com/license/
          Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=BizPage
        -->
        Designed by <a href="#"></a>
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <!-- Uncomment below i you want to use a preloader -->
  <!-- <div id="preloader"></div> -->
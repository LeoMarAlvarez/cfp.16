<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="mobile-web-app-capable" content="yes">
    <meta content="" name="keywords">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    @include('layouts.landing.links')
</head>
<body>
    @yield('nav')

    @yield('slider')

    <div id="main">
        @yield('content')
    </div>

    @include('layouts.landing.footer')
    @include('layouts.landing.scripts')
</body>
</html>

  <!-- JavaScript Libraries -->
  <script src="{{ asset('bizpage/lib/jquery/jquery.min.js')}}"></script>
  <script src="{{ asset('bizpage/lib/jquery/jquery-migrate.min.js')}}"></script>
  <script src="{{ asset('bizpage/lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{ asset('bizpage/lib/easing/easing.min.js')}}"></script>
  <script src="{{ asset('bizpage/lib/superfish/hoverIntent.js')}}"></script>
  <script src="{{ asset('bizpage/lib/superfish/superfish.min.js')}}"></script>
  <script src="{{ asset('bizpage/lib/wow/wow.min.js')}}"></script>
  <script src="{{ asset('bizpage/lib/waypoints/waypoints.min.js')}}"></script>
  <script src="{{ asset('bizpage/lib/counterup/counterup.min.js')}}"></script>
  <script src="{{ asset('bizpage/lib/owlcarousel/owl.carousel.min.js')}}"></script>
  <script src="{{ asset('bizpage/lib/isotope/isotope.pkgd.min.js')}}"></script>
  <script src="{{ asset('bizpage/lib/lightbox/js/lightbox.min.js')}}"></script>
  <script src="{{ asset('bizpage/lib/touchSwipe/jquery.touchSwipe.min.js')}}"></script>
  <!-- Contact Form JavaScript File -->
  <script src="{{ asset('bizpage/contactform/contactform.js')}}"></script>

  <!-- Template Main Javascript File -->
  <script src="{{ asset('bizpage/js/main.js')}}"></script>

  @yield('otrosscripts')
  
  <script src="{{ asset('solid/assets/js/jquery.scrollex.min.js')}}"></script>
  <script src="{{ asset('solid/assets/js/browser.min.js')}}"></script>
  <script src="{{ asset('solid/assets/js/breakpoints.min.js')}}"></script>
  <script src="{{ asset('solid/assets/js/util.js')}}"></script>
  <script src="{{ asset('solid/assets/js/main.js')}}"></script>
  
  <!-- Favicons -->
  <link href="{{ asset('img/favicon.ico')}}" rel="icon">
  <link href="{{ asset('bizpage/img/apple-touch-icon.png')}}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="{{ asset('bizpage/lib/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="{{ asset('bizpage/lib/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
  <link href="{{ asset('bizpage/lib/animate/animate.min.css')}}" rel="stylesheet">
  <link href="{{ asset('bizpage/lib/ionicons/css/ionicons.min.css')}}" rel="stylesheet">
  <link href="{{ asset('bizpage/lib/owlcarousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
  <link href="{{ asset('bizpage/lib/lightbox/css/lightbox.min.css')}}" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="{{ asset('bizpage/css/style.css')}}" rel="stylesheet">

  @yield('cssOffer')

  @yield('otroslinks')

  <link href="{{ asset('solid/assets/css/main.css')}}" rel="stylesheet"/>
  <noscript><link href="{{ asset('solid/assets/css/noscript.css')}}" rel="stylesheet"/></noscript>

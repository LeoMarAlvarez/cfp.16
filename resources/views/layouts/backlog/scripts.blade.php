	<!-- REQUIRED SCRIPTS -->

	<!-- jQuery -->
    <script src="{{ asset('adminlte/plugins/jquery/jquery.min.js')}}"></script>

    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    {{-- <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script> --}}

    <!-- Select2 -->
    <script src="{{ asset('adminlte/plugins/select2/js/select2.full.min.js') }}"></script>

	<!-- Bootstrap 4-->
    <script src="{{ asset('adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Moment -->
    <script src="{{ asset('adminlte/plugins/moment/moment.min.js') }}"></script>
    
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="{{ asset('adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>

    
	<!-- AdminLTE -->
	<script src="{{ asset('adminlte/dist/js/adminlte.js')}}"></script>

	<!-- OPTIONAL SCRIPTS -->

    <script src="{{ asset('adminlte/dist/js/demo.js')}}"></script>

    <!-- Summernote -->
    <script src="{{ asset('adminlte/plugins/summernote/summernote-bs4.min.js') }}"></script>

	<!--SweetAlert-->
    <script src="{{ asset('plugins/sweetalert/sweetalert.min.js') }}"></script>

    <script src="{{ asset('plugins/bootbox.min.js') }}"></script>

    @yield('scripts')

    <script>
        function alerta(message){
            var alertType = message['alert-type'];
            var icon = "info";

            var message = message['message'];

            var title = 'info';

            var button = false;

            switch (alertType) {
                case 'info':
                    title = 'Informacion';
                    break;
                case 'warning':
                    title = 'Aviso';
                    icon='warning';
                    break;
                case 'success_ok':
                    title='Perfecto';
                    icon='success';
                    button = true;
                    break;
                case 'success':
                    title = 'Perfecto';
                    icon='success';
                    break;
                case 'error':
                    title = 'Oops';
                    icon='error';
                    button=true;
                    break;
            }
            if(button)
            {
                swal({
                    type:icon,
                    title:title,
                    text:message,
                    showCancelButton:false,
                    showConfirmButton:button,
                });
            }else{
                swal({
                    type:icon,
                    title:title,
                    text:message,
                    showCancelButton:false,
                    showConfirmButton:button,
                    timer:2500
                });
            }
        }

        function confirmarBorrado(url){
            swal({
                title: "Esta seguro de borrar el registro?",
                text: "Se borrara el registro",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Borrar",
                closeOnConfirm: false,
                closeOnCancel:false
            }, function (isConfirm) {
                if(isConfirm) {
                    $.ajax({
                        url:url,
                        method:'DELETE',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success:function(data){
                            alerta(data);
                            listar($('#contenedor').data('list'),'');
                        },
                        error:function(data){
                            var message={
                                'alert-type':'error',
                                'message':'No se pudo realizar la operación. Contactate con el administrador del sitio'
                            };
                            alerta(message);
                        }
                    });
                }else{
                    swal({
                        title:'Cancelado',
                        text:'No se borrará el registro',
                        type:'error',
                        showConfirmButton:false,
                        timer:1500
                    });
                }
            });
        }

        $(document).on('click','.pagination li a',function(e){
            e.preventDefault();
            var url = $(this).attr('href');
            $.ajax({
                url:url,
                type:'get',
                success: function(data){
                    $('#contenedor').empty().html(data);
                }
            });
        });
    </script>

	


<table class="table table-striped">
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Profesor</th>
            <th>Acción</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($commissions as $c)
            <tr>
                <td>{{$c->name}}</td>
                <td>{{$c->teacher->name.' '.$c->teacher->personalData->last_name}}</td>
                <td>
                    <a href="{{route('commission.edit',$c->id)}}"
                        class="btn btn-success btn-xs editarComision"
                        data-send="{{route('commission.update',$c->id)}}"
                        title="Editar">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{route('commission.destroy',$c->id)}}"
                        class="btn btn-danger btn-xs borrarComision"
                        title="Borrar">
                        <i class="fa fa-trash"></i>
                    </a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

{!! $commissions->links() !!}
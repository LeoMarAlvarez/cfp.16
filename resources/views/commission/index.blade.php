
    <div class="card">
        <div class="card-header row">
            <div class="col-md-4">
                <a href="{{route('commission.create')}}"
                    data-send="{{route('commission.store')}}"
                    class="btn btn-primary btn-md"
                    id="nuevaComision">
                    <i class="fa fa-plus"></i>
                    Nueva Comisión
                </a>
            </div>
            <div class="col-md-8">
                <div class="d-none" id="accionar" data-send="a" data-type="a"></div>
            </div>
        </div>
        <div class="card-body"
            id="cajaComisiones"
            data-list="{{route('commission.list')}}"
            data-commission="{{$id}}">
        </div>
    </div>
    <script src="{{asset('js/commission.js')}}"></script>
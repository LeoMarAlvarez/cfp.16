<form autocomplete="off" id="formularioComision">
    @csrf
    <div class="form-group row">
        <label for="name" class="col-lg-3">Nombre</label>
        <input 
            type="text" 
            value="{{isset($comision)? $comision->name:''}}"
            name="name" 
            maxlength="20"
            required
            class="col-lg-9 form-control">
    </div>
    <div class="form-group row">
        <label for="teacher" class="col-lg-3">Profesor</label>
        <select name="teacher" class="select2 form-control col-lg-9" required>
            @foreach ($profes as $p)
                <option {{($p->id==$profe)? 'selected':''}}
                    value="{{$p->id}}">{{$p->name.' '.$p->personalData->last_name}}
                </option>
            @endforeach
        </select>
    </div>
    <div class="form-group row">
        <label for="schedule" class="col-lg-3">Horarios</label>
        <textarea name="schedule"
        required
        maxlength="255"
        class="textarea col-lg-9 form-control">{{isset($comision)? $comision->schedule:''}}</textarea>
    </div>
    <div class="form-group text-center">
        <button 
            type="button"
            class="btn btn-default"
            id="btnCancelarComision">
            <i class="fa fa-undo"></i> Cancelar
        </button>
        <button 
            type="button"
            class="btn btn-primary"
            id="btnGuardarComision">
            <i class="fa fa-save"></i> Guardar
        </button>
    </div>
    <div class="align-content-center" id="response"></div>
</form>


<script>
    $(document).ready(function() {
        $('.dropify').dropify({
            messages: {
                'default' : 'Mover la imagen aquí o hacer click',
                'replace' : 'Mover la imagen aquí o hacer clik para reemplazar',
                'remove' : 'Eliminar',
                'error' : 'Ooops, ocurrió un error.'
            }
        });

        $('.textarea').summernote({
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']]
            ]
        });

        $('.select2').select2();
    });
</script>
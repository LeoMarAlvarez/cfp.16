@extends('layouts.landing.app')

@section('nav')
	@include('layouts.landing.nav2')
@stop

@section('content')
	{{ $i=0}}{{ $flag=true }}
	<!-- One -->
	@while($flag)
		@if($i<count($ofertas))>
		<section id="one" class="wrapper spotlight style1">
			<div class="inner">
				<a href="#" class="image"><img src="{{ Storage::url($ofertas[$i]->photo) }}" alt="" /></a>
				<div class="content">
					<h2 class="major">{{ $ofertas[$i]->name }}</h2>
					<p>{!! $ofertas[$i]->graduate_profile !!}</p>
					<a href="#" class="special">Learn more</a>
				</div>
			</div>
		</section>
		{{ $i++ }}
		@else
		{{ $flag=false }}
		@endif

		<!-- Two -->
		@if($i<count($ofertas))>
		<section id="two" class="wrapper alt spotlight style2">
			<div class="inner">
				<a href="#" class="image"><img src="{{ Storage::url($ofertas[$i]->photo) }}" alt="" /></a>
				<div class="content">
					<h2 class="major">{{ $ofertas[$i]->name }}</h2>
					<p>{!! $ofertas[$i]->graduate_profile !!}</p>
					<a href="#" class="special">Learn more</a>
				</div>
			</div>
		</section> 
		{{ $i++ }}
		@else
		{{ $flag=false }}
		@endif
	@endwhile

@endsection

<table class="table table-bordered table-striped table-hover">
    <thead class="thead-dark">
        <tr>
            <th>Nombre</th>
            <th>Duración</th>
            <th>Comisiones</th>
            <th>Opciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($ofertas as $ao)
            <tr>
                <td>{{ $ao->name }}</td>
                <td>{{ $ao->duration }}</td>
                @if(count($ao->commissions)>0)
                    <td>
                        @if($ao->commissions()->count()>0)
                            <table class="table table-sm table-light text-center">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Profe</th>
                                        <th>Horario</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($ao->commissions as $co)
                                    <tr>
                                        <td>{{ $co->name }}</td>
                                        <td>{{ $co->teacher->name . ' ' . $co->teacher->personalData->last_name }}</td>
                                        <td>{!! $co->schedule !!}</td>
                                    </tr>
                                    @endforeach        
                                </tbody>
                            </table>
                            @endif
                    </td>
                @else
                    <td><span class="text-muted">-</span></td>
                @endif
                <td style="width: 100px">
                    <a href="{{route('commission.index')}}" 
                        class="btn btn-light btn-xs editar-comisiones"
                        data-commission="{{$ao->id}}"
                        title="Editar Comisiones">
                        <i class="fa fa-list"></i>
                    </a> 
                    <a href="{{route('offer.edit',$ao->id)}}" 
                        class="btn btn-success btn-xs editar"
                        data-send="{{route('offer.update',$ao->id)}}"
                        data-title="Editar {{$ao->name}}"
                        title="Editar">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{route('offer.destroy',$ao->id)}}" 
                        class="btn btn-danger btn-xs borrar"
                        title="Eliminar">
                        <i class="fa fa-trash"></i>
                    </a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
<div class="card-footer">
    {!! $ofertas->links() !!}
</div>
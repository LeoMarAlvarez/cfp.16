@extends('layouts.backlog.app')

@section('links')
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/select2/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">

    <link rel="stylesheet" href="{{ asset('plugins/dropify/css/dropify.min.css')}}">
@endsection

@section('header-title')
    <h2>Ofertas Académicas</h2>
@endsection

@section('header-button')
    <a 
        href="{{route('offer.create')}}" 
        class="btn btn-primary btn-md" 
        data-title="Nueva Oferta"
        id="nuevo"
        data-send="{{ route('offer.store') }}">
        <i class="fa fa-plus"></i>
        Nuevo
    </a>
@endsection

@section('content')
    <div class="card">
        <div class="card-header text-left">
            <input type="text" autocomplete="off" id="search" placeholder="Buscar" class="form-control col-lg-4">
        </div>
        <div 
            class="card-body"
            id="contenedor" 
            data-list="{{route('offer.list')}}">
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('plugins/dropify/js/dropify.min.js')}}"></script>
    <script src="{{ asset('js/offer.js') }}"></script>
@endsection
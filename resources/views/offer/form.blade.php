<div class="container-fluid">
    <form method="post" autocomplete="off" enctype="multipart/form-data" id="formulario">
        <div class="row">
            <div class="col-lg-12">
                <label for="name">Nombre</label>
                <input 
                    type="text" 
                    name="name" 
                    id="name"
                    placeholder="Ej: Repostería"
                    maxlength="200"
                    value="{{ isset($oferta)? $oferta->name:'' }}"
                    class="form-control">
            </div>
            <div class="col-lg-6">
                <label for="duration">Duración</label>
                <input 
                    type="text" 
                    name="duration" 
                    id="duration"
                    placeholder="Ej: 1 año - 10 meses"
                    maxlength="50"
                    value="{{ isset($oferta)? $oferta->duration:'' }}"
                    class="form-control">
            </div>
            <div class="col-lg-6">
                <label for="photo">Foto</label>
                <input 
                    type="file" 
                    name="photo" 
                    id="photo"
                    accept="image/*"
                    data-default-file="{{isset($oferta)? Storage::url($oferta->photo):''}}"
                    class="form-control dropify">
            </div>
            <div class="col-lg-12">
                <div class="card collapsed-card">
                    <div class="card-header">
                        <h5 class="card-title">Perfil del graduado</h5>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div></div>
                    
                    <div class="card-body">
                        <textarea 
                            name="graduate_profile" 
                            id="graduate_profile" 
                            class="textarea form-control">{{isset($oferta)?$oferta->graduate_profile:''}}</textarea>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card collapsed-card">
                    <div class="card-header">
                        <h5 class="card-title">Contenido de la materia</h5>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div></div>
                    
                    <div class="card-body">
                        <textarea 
                            name="content" 
                            id="content" 
                            class="textarea form-control">{{isset($oferta)?$oferta->content:''}}</textarea>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="card collapsed-card">
                    <div class="card-header">
                        <h5 class="card-title">Descripción</h5>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div></div>
                    
                    <div class="card-body">
                        <textarea 
                            name="description" 
                            id="description" 
                            class="textarea form-control">{{isset($oferta)?$oferta->description:''}}</textarea>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="attached_program">Programa de la oferta</label>
                    <input 
                        type="file" 
                        name="attached_program" 
                        id="attached_program" 
                        data="{{isset($oferta)? Storage::url($oferta->attached_program):''}}"
                        accept="application/pdf"
                        class="form-control-file">
                </div>
            </div>
            <div class="align-content-center" id="response"></div>
        </div>
    </form>
</div>

<script>
    $(document).ready(function() {
        let i = 0;
        $('.dropify').dropify({
            messages: {
                'default' : 'Mover la imagen aquí o hacer click',
                'replace' : 'Mover la imagen aquí o hacer clik para reemplazar',
                'remove' : 'Eliminar',
                'error' : 'Ooops, ocurrió un error.'
            }
        });

        $('.textarea').summernote({
            tabsize: 2,
            height: 120,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']]
            ]
        });
    });
</script>


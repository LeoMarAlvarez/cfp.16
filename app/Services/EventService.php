<?php

namespace App\Services;
use Illuminate\Support\Facades\Cache;
use App\User;
use App\Models\AcademicOffer;
use App\Models\Image;
use App\Models\Event;

class EventService 
{

    public static function all($request)
    {
        return Event::search($request['search'])->paginate(10);
    }

    public static function find($id)
    {
        return Event::findOrFail($id);
    }

    public static function store($data)
    {}

    public static function update($data, $id)
    {}

    public static function destroy($id)
    {
        $evento = Event::findOrFail($id);
        $title = $evento->title;
        $evento->delete();
        return $title;
    }
}
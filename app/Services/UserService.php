<?php

namespace App\Services;
use Illuminate\Support\Facades\Cache;
use App\User;
use App\Models\PersonalData;
use App\Models\Role;

class UserService 
{
    public static function teachers()
    {
        return User::role('Profesor')->get();
    }

    public static function all($request)
    {
        return User::search($request['search'])->paginate(3);
    }

    public static function find($id)
    {
        return User::findOrFail($id);
    }

    public static function store($data)
    {
        $user = User::create([
            'name'=>$data['name'],
            'email'=>$data['email'],
            'password'=>bcrypt($data['dni'])
        ]);

        $ps = PersonalData::create([
            'last_name' => empty($data['last_name'])? '':$data['last_name'],
            'dni' => $data['dni'],
            'number_file' => empty($data['number_file'])? '-':$data['number_file'],
            'phone' => empty($data['phone'])? '':$data['phone'],
            'user_id' => $user->id
        ]);

        $user->assignRole(Role::findOrFail($data['role']));

        return $user;

    }

    public static function update($data, $id)
    {
        $user = User::findOrFail($id);
        $user->name = $data['name'];
        $user->email = $data['email'];

        $user->personalData->last_name = isset($data['last_name'])? $data['last_name']:'';
        $user->personalData->dni = isset($data['dni'])? $data['dni']:'';
        $user->personalData->number_file = isset($data['number_file'])? $data['number_file']:'';
        $user->personalData->phone = isset($data['phone'])? $data['phone']:'';
        $user->personalData->save();
        
        if($data['role'] != $user->getRole()->id)
        {
            $user->removeRole($user->getRole());
            $user->assignRole(Role::findOrFail($data['role']));
        }
        $user->save();
        return $user;
    }

    public static function destroy($id)
    {
        $u = User::findOrFail($id);
        $user['name'] = $u->name;
        $user['rol'] = $u->getRoleName();
        $u->delete();
        return $user;
    }
}
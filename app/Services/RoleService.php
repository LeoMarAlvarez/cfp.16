<?php

namespace App\Services;
use Illuminate\Support\Facades\Cache;
use App\Models\Role;

class RoleService 
{
    public static function getRols()
    {
        return Role::orderBy('name')->get();
    }

    public static function all($request)
    {}

    public static function find($id)
    {}

    public static function findByName($name)
    {
        return Role::where('name',$name)->first();
    }

    public static function store($data)
    {}

    public static function update($data, $id)
    {}

    public static function destroy($id)
    {}
}
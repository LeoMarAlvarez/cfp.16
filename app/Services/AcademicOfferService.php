<?php

namespace App\Services;
use Illuminate\Support\Facades\Cache;
use App\User;
use App\Models\Commission;
use App\Models\Image;
use App\Models\AcademicOffer;
use Illuminate\Support\Facades\Storage;

class AcademicOfferService 
{


    public static function allOffers(){
        return AcademicOffer::all();
    }

    public static function all($request)
    {
        return AcademicOffer::search($request['search'])->paginate(3);
    }

    public static function find($id)
    {
        return AcademicOffer::findOrFail($id);
    }

    public static function store($data)
    {
        $oferta = AcademicOffer::create([
            'name'=>$data['name'],
            'graduate_profile'=> isset($data['graduate_profile'])? $data['graduate_profile']:'',
            'description'=> isset($data['description'])? $data['description']:'',
            'duration'=>$data['duration'],
            'content'=> isset($data['content'])? $data['content']:'',
            'attached_program'=>isset($data['attached_program'])?$data['attached_program']->store('public/academic_offers/'.$data['name']):'',
            'photo'=>isset($data['photo'])?$data['photo']->store('public/academic_offers/'.$data['name']):'',
        ]);
        return $oferta;
    }

    public static function update($data, $id)
    {
        $oferta = AcademicOffer::findOrFail($id);
        $oferta->name = $data['name'];
        $oferta->graduate_profile = $data['graduate_profile'];
        $oferta->description = $data['description'];
        $oferta->duration = $data['duration'];
        $oferta->content = $data['content'];
        if(isset($data['attached_program']))
        {
            $oferta->attached_program = $data['attached_program']->store('public/academic_offers/'.$data['name']);
        }
        if(isset($data['photo']))
        {
            $oferta->photo = $data['photo']->store('public/academic_offers/'.$data['name']);
        }
        $oferta->save();

        return $oferta;
    }

    public static function destroy($id)
    {
        $offer = AcademicOffer::findOrFail($id);
        $name = $offer->name;
        $offer->delete();
        return $name;
    }
}
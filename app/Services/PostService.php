<?php

namespace App\Services;
use Illuminate\Support\Facades\Cache;
use App\User;
use App\Models\AcademicOffer;
use App\Models\Image;
use App\Models\Post;

class PostService 
{

    public function all($request)
    {
        return Post::search($request['search'])->paginate(10);
    }

    public function find($id)
    {
        return Post::findOrFail($id);
    }

    public function store($data)
    {}

    public function update($data, $id)
    {}

    public function destroy($id)
    {
        $post = Post::findOrFail($id);
        $title = $post->title;
        $post->delete();
        return $title;
    }
}
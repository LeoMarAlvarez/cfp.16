<?php

namespace App\Services;
use App\Models\AcademicOffer;
use App\Models\Commission;

class CommissionService {

    public static function commissionsByOffer($request)
    {
        return Commission::byOffer($request['id'])->paginate(5);
    }

    public static function find($id)
    {
        return Commission::findOrFail($id);
    }

    public static function store($data)
    {
        $c = Commission::create([
            'name'=>$data['name'],
            'offer_id'=>$data['offer_id'],
            'teacher_id'=>$data['teacher'],
            'schedule'=>$data['schedule']
        ]);

        return $c;
    }

    public static function update($data, $id)
    {
        $c = Commission::findOrFail($id);
        $c->name=$data['name'];
        $c->schedule = $data['schedule'];
        $c->offer_id=$data['offer_id'];
        $c->teacher_id=$data['teacher'];
        $c->save();

        return $c;
    }

    public static function destroy($id)
    {
        $c = Commission::findOrFail($id);
        $name = $c->name;
        $c->delete();
        return $name;
    }
}
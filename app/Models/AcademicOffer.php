<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AcademicOffer extends Model
{
    protected $table = 'academic_offers';
    
    protected $primaryKey = 'id';
    
    protected $fillable = [
        'name',
        'graduate_profile',
        'description',
        'duration',
        'content',
        'attached_program',
        'photo'
    ];

    public function commissions()
    {
        return $this->hasMany(Commission::class, 'offer_id');
    }

    public function images()
    {
        return $this->hasMany(Image::class, 'academic_offer_id');
    }

    public function posts()
    {
        return $this->belongsToMany(Post::class, 'academic_offers_by_posts', 'academic_offer_id', 'post_id');
    }

    public function events()
    {
        return $this->belongsToMany(Event::class, 'academic_offers_by_events', 'academic_offer_id', 'event_id');
    }

    public function scopeSearch($query, $name)
    {
        if(empty($name)){
            return $query->orderBy('name','ASC');
        }
        
        return $query->where('name','like','%'.$name.'%');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class PersonalData extends Model
{
    protected $table = 'personals_data';
    protected $primaryKey = 'id';
    protected $fillable = [
        'last_name',
        'dni',
        'phone',
        'number_file',
        'photo',
        'description',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}

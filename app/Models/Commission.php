<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Commission extends Model
{
    protected $table = "commissions";

    protected $primaryKey = "id";

    protected $fillable = [
        'name',
        'schedule',
        'teacher_id',
        'offer_id'
    ];

    public $timestamps = false;

    public function teacher()
    {
        return $this->belongsTo(User::class, 'teacher_id');
    }

    public function academicOffer()
    {
        return $this->belongsTo(AcademicOffer::class,'offer_id');
    }

    public function scopeByOffer($query, $offer)
    {
        return $query->where('offer_id',$offer);
    }
}

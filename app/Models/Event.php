<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Event extends Model
{
    protected $table = 'events';
    
    protected $primaryKey = 'id';
    
    protected $fillable = [
        'title',
        'url',
        'description',
        'date',
        'start_time',
        'place',
        'photo'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function academicOffers()
    {
        return $this->belongsToMany(AcademicOffer::class, 'academic_offers_by_events','event_id','academic_offer_id');
    }

    public function scopeSearch($query, $search)
    {
        if(isset($search)){
            return;
        }
        $query->where('title','like','%'.$search.'%');
    }
}

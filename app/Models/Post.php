<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Post extends Model
{
    protected $table = 'posts';
    
    protected $primaryKey = 'id';
    
    protected $fillable = [
        'title',
        'url',
        'description',
        'photo',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function images()
    {
        return $this->hasMany(Image::class, 'post_id');
    }

    public function academicOffers()
    {
        return $this->belongsToMany(AcademicOffer::class, 'academic_offers_by_posts','post_id', 'academic_offer_id');
    }

    public function scopeSearch($query, $title)
    {
        if(empty($title))
        {
            return;
        }
        $query->where('title','like',"%$title%");
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CommissionRequest;
use App\Services\CommissionService;
use App\Services\UserService;

class CommissionController extends Controller
{
    protected $cs,$us;

    public function __construct(CommissionService $cos, UserService $uss)
    {
        $this->cs = $cos;
        $this->us = $uss;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = request('id');
        return view('commission.index',compact('id'));
    }

    public function list()
    {
        $commissions = $this->cs::commissionsByOffer(request());
        return view('commission.list',compact('commissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $profes = $this->us::teachers();
        $profe = null;
        return view('commission.form',compact('profes','profe'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommissionRequest $request)
    {
        $data = $request->except('_token');
        $c = $this->cs::store($data);
        $message=[
            'alert-type'=>'success',
            'message'=>'La comisión '.$c->name.' se registró correctamente para la oferta '.$c->academicOffer->name
        ];

        return $message;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $comision = $this->cs::find($id);
        $profes = $this->us::teachers();
        $profe = $comision->teacher->id;
        return view('commission.form',compact('comision','profes','profe'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CommissionRequest $request, $id)
    {
        $data = $request->except('_token');
        $c = $this->cs::update($data, $id);
        $message=[
            'alert-type'=>'success',
            'message'=>'La comisión '.$c->name.' se actualizó correctamente'
        ];

        return $message;   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $nombre = $this->cs::destroy($id);
        $message=[
            'alert-type'=>'success',
            'message'=>'La comisión '.$nombre.' se eliminó correctamente'
        ];

        return $message;
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Services\UserService;
use App\Services\RoleService;
use App\User;

class UserController extends Controller
{
    protected $us, $rs;

    public function __construct(RoleService $roleS, UserService $userS)
    {
        $this->us = $userS;
        $this->rs = $roleS;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $users = $this->us::all(request());

        return view('user.list',compact('users'));
    }

    public function index(){
        return view('user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = $this->rs::getRols();

        return view('user.form',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $data = $request->except('_token');
        $user = $this->us::store($data);
        $message = [
            'alert-type'=>'success',
            'message'=>'El '.$user->getRoleName().' '.$user->name.' se registró correctamente'
        ];
        return $message;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $usuario = $this->us::find($id);

        return view('user.show',compact('usuario'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario = $this->us::find($id);
        $rol = $usuario->getRole()->id;
        $roles = $this->rs::getRols();

        return view('user.form',compact('usuario','roles','rol'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        $data = $request->except('_token');
        $usuario = $this->us::update($data, $id);
        $message=[
            'alert-type'=>'success',
            'message'=>'Los cambios fueron guardados correctamente'
        ];

        return $message;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $datos = $this->us::destroy($id);
        $message = [
            'alert-type'=>'success',
            'message'=>'El '.$datos['rol'].' '.$datos['name'].' se borró correctamente'
        ];
        return $message;
    }
}

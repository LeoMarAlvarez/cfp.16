<?php

namespace App\Http\Controllers;

use App\models\Post;
use Illuminate\Http\PostsRequest;
use App\Services\PostService;
use App\Services\AcademicOfferService;
use App\Services\UserService;


class PostController extends Controller
{
    protected $postService, $academicOfferService, $userService;

    public function __construct(PostService $ps, AcademicOfferService $aos, UserService $us)
    {
        $postService = $ps;
        $academicOfferService = $aos;
        $userService = $us;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = $this->postService::all(request());

        return view('post.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ofertas = $this->academicOfferService::pluck();
        return view('post.create',compact('ofertas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        dd($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = $this->postService::find($id);

        return view('post.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = $this->postService::find($id);
        $ofertas = $this->academicOfferService::pluck();
        return view('post.edit',compact('post','ofertas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, $id)
    {
        dd($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $nombre = $this->postService::destroy($id);

    }
}

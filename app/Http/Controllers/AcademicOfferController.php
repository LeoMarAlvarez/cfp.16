<?php

namespace App\Http\Controllers;

use App\models\AcademicOffer;
use App\Http\Requests\OfferRequest;
use App\Services\AcademicOfferService;
use App\Services\UserService;

class AcademicOfferController extends Controller
{
    protected $offerService, $userService;

    public function __construct(AcademicOfferService $aos, UserService $us)
    {
        $this->offerService = $aos;
        $this->userService = $us;
    }

    public function list()
    {
        $ofertas = $this->offerService::all(request());
        return view('offer.list',compact('ofertas'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('offer.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('offer.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OfferRequest $request)
    {
        $data = $request;
        $oferta = $this->offerService::store($data);
        $message=[
            'alert-type'=>'success',
            'message'=>$oferta->name.' se agregó correctamente'
        ];

        return $message;
    }

    /**
     * Display the specified resource.
     *
     * @param  id de academicOffer $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $oferta = $this->offerService::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  id de academic offer $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $oferta = $this->offerService::find($id);

        return view('offer.form',compact('oferta'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\AcademicOffer  $academicOffer
     * @return \Illuminate\Http\Response
     */
    public function update(OfferRequest $request, $id)
    {
        $oferta = $this->offerService::update($request,$id);
        $message = [
            'alert-type'=>'success',
            'message'=>$oferta->name.' se actualizó correctamente'
        ];
        return $message;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\AcademicOffer  $academicOffer
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $nombre = $this->offerService::destroy($id);
        $message = [
            'alert-type'=>'success',
            'message'=>$nombre.' se eliminó correctamente'
        ];
        return $message;
    }

    public function showAllOffers()
    {
        $ofertas = $this->offerService::allOffers();

        return view('offer.showalloffers', compact('ofertas'));
    }
}

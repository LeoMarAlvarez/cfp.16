<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'last_name'=>'max:100',
            'dni'=>'required|unique:personals_data,dni,'.$this->route('usuario'),
            'email'=>'required|unique:users,email,'.$this->route('usuario'),
            'role'=>'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required'=>'El nombre es obligatorio',
            'last_name.max'=>'El apellido no debe superar los 100 caracteres',
            'dni.required'=>'El DNI es obligatorio',
            'dni.unique'=>'El dni está asociado a otro usuario',
            'email.required'=>'El email es obligatorio',
            'email.unique'=>'El email está asociado a otro usuario',
            'role.required'=>'Debes seleccionar un tipo de usuario'
        ];
    }
}

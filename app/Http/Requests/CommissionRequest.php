<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommissionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'teacher'=>'required',
            'schedule'=>'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' =>'El campo nombre no debe estar vacío',
            'teacher.required' =>'El campo Profe no debe estar vacío',
            'schedule.required' =>'El campo Horario no debe estar vacío',
        ];
    }
}

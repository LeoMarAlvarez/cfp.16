<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OfferRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
        public function rules()
        {
           return [
            'name'=>'required',
            'description'=>'required',
            'duration'=>'required',
            'graduate_profile'=>'required'
           ];
        }

    public function messages()
    {
        return [
            'name.required'=>'El campo nombre no debe estar vacío',
            'description.required'=>'El campo Descripción no debe estar vacío',
            'duration.required'=>'El campo duración no debe estar vacío',
            'graduate_profile.required'=>'El campo perfil del graduado no debe estar vacío',
        ];
    }
}

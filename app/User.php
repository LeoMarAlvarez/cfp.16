<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\PersonalData as PersonalData;
use App\Models\Event;
use App\Models\Post;
use App\Models\Commission;
use Spatie\Permission\Traits\HasRoles;
use App\Models\Role;


class User extends Authenticatable
{
    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function personalData()
    {
        return $this->hasOne(PersonalData::class, 'user_id');
    }

    public function events()
    {
        return $this->hasMany(Event::class, 'user_id');
    }

    public function posts()
    {
        return $this->hasMany(Post::class, 'user_id');
    }

    public function commissions()
    {
        return $this->hasMany(Commission::class, 'user_id');
    }

    public function getRoleName()
    {
        return $this->getRoleNames()->first();
    }

    public function getRole()
    {
        return Role::where('name',$this->getRoleName())->first();
    }

    public function scopeSearch($query, $search)
    {
        if(empty($search)){
            return $query->orderBy('name','ASC');
        }
        
        $query->where('name','like','%'.$search.'%')
            ->orWhere('email', 'like', '%'.$search.'%')
            ->orderBy('name','ASC');
    }
}
